import tensorflow as tf
import warnings
warnings.filterwarnings("ignore")
import numpy as np
from utils.config import *
from utils.data_loader import load_dataset, build_dataset, preprocess_sentence
from utils.wv_loader import load_word2vec_file, get_vocab



def evaluate(sentence):
    attention_plot = np.zeros((max_length_targ, max_length_inp+ 2))

    inputs = preprocess_sentence(sentence, max_length_inp, vocab)

    inputs = tf.convert_to_tensor(inputs)

    result = ''

    hidden = [tf.zeros((1, units))]
    enc_out, enc_hidden = encoder(inputs, hidden)

    dec_hidden = enc_hidden

    dec_input = tf.expand_dims([vocab['<START>']], 0)

    for t in range(max_length_targ):
        predictions, dec_hidden, attention_weights = decoder(dec_input,
                                                             dec_hidden,
                                                             enc_out)

        # storing the attention weights to plot later on
        attention_weights = tf.reshape(attention_weights, (-1,))

        attention_plot[t] = attention_weights.numpy()
        predicted_id = tf.argmax(predictions[0]).numpy()

        result += reverse_vocab[predicted_id] + ' '
        if reverse_vocab[predicted_id] == '<STOP>':
            return result, sentence, attention_plot

        # the predicted ID is fed back into the model
        dec_input = tf.expand_dims([predicted_id], 0)

    return result, sentence, attention_plot



def plot_attention(attention, sentence, predicted_sentence):
    fig = plt.figure(figsize=(10,10))
    ax = fig.add_subplot(1, 1, 1)
    ax.matshow(attention, cmap='viridis')

    fontdict = {'fontsize': 14,'fontproperties':font}

    ax.set_xticklabels([''] + sentence, fontdict=fontdict, rotation=90)
    ax.set_yticklabels([''] + predicted_sentence, fontdict=fontdict)

    ax.xaxis.set_major_locator(ticker.MultipleLocator(1))
    ax.yaxis.set_major_locator(ticker.MultipleLocator(1))

    plt.show()



def translate(sentence):
    result, sentence, attention_plot = evaluate(sentence)

    print('Input: %s' % (sentence))
    print('Predicted translation: {}'.format(result))

    attention_plot = attention_plot[:len(result.split(' ')), :len(sentence.split(' '))]
    plot_attention(attention_plot, sentence.split(' '), result.split(' '))



if __name__ == "__main__":
    print("load vocab")
    vocab, reverse_vocab = get_vocab(save_wv_model_path)
    # 加载数据集
    print("load dataset")
    train_X, train_Y, test_X = load_dataset()
    # 输入的长度
    max_length_inp = train_X.shape[1]
    # 输出的长度
    max_length_targ = train_Y.shape[1]

    sentence = '漏机油 具体 部位 发动机 变速器 正中间 位置 拍 中间 上面 上 已经 看见'
    print("Input: ", sentence)
    translate(sentence)